package com.example.thiba.livre;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent act = new Intent(this, ListviewLivre.class);
        startActivity(act);

    }
    //on va traiter le resultat d'un formulaire au bout d'un moment
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // REQUEST_CODE is defined above
        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE) {
            // Extract object from result extras
            // Make sure the key here matches the one specified in the result passed from ActivityTwo.java
            MyParcelable object = data.getParcelableExtra("myDataKey");
        }
        private static final int CODE_LECTURE = 42;
...
/**
 * Fires an intent to spin up the "file chooser" UI and select an image.
 */
        public void performFileSearch() {

            // ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file
            // browser.
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);

            // Filter to only show results that can be "opened", such as a
            // file (as opposed to a list of contacts or timezones)
            intent.addCategory(Intent.CATEGORY_OPENABLE);

            // Filter to show only images, using the image MIME data type.
            // If one wanted to search for ogg vorbis files, the type would be "audio/ogg".
            // To search for all documents available via installed storage providers,
            // it would be "*/*".
            intent.setType("image/*");

            startActivityForResult(intent, CODE_LECTURE);
        }


        @Override
        public void onActivityResult(int requestCode, int resultCode,
        Intent resultData) {
            if (requestCode == CODE_LECTURE && resultCode == Activity.RESULT_OK) {
                Uri uri = null;
                if (resultData != null) {
                    uri = resultData.getData();
                    Log.i(TAG, "Uri: " + uri.toString());
                    showImage(uri);
                }
            }
        }

    }
